import React, { useState } from 'react';

import AppHeader from './components/AppHeader';
import AppFooter from './components/AppFooter';
import Editor from './components/Editor';

import './style/app.scss'
import Alert from './components/Alert';

function App() {
  const [code, setCode] = useState('');
  const [convertedCode, setConvertedCode] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const convertJsonToJs = () => {
    try {
      JSON.parse(code);
      const converted = code
        .replace(/"/g, "'")
        .replace(/'([a-zA-Z0-9$_]+)':/g, '$1:')
      setConvertedCode(converted);
    } catch (err) {
      setErrorMessage(err.message)
    }
  }

  const updateCode = (({ target }) => {
    setConvertedCode('');
    setErrorMessage('');
    setCode(target.value);
  });

  return (
    <div className="App">
      <AppHeader />

      <div className="container">
        <div className="editors flex">
          <Editor
            value={code}
            onChange={updateCode}
          />
          <div className="json-output">
            <Editor
              value={convertedCode}
              onFocus={({ target }) => target.select()}
            />
          </div>
        </div>
      </div>

      <AppFooter onClick={convertJsonToJs} />
      { errorMessage ?  <Alert message={errorMessage} onClick={() => setErrorMessage('')} /> : '' }
    </div>
  );
}

export default App;

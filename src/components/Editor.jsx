import React from 'react';
import PropTypes from 'prop-types';

import './Editor.scss';

const Editor = ({ onChange, value, disabled, onFocus }) => {
  return (
    <section className="editor">
      <textarea
        value={value}
        disabled={disabled}
        onChange={onChange}
        onFocus={onFocus}
      />
    </section>
  );
}

Editor.propTypes = {
  value: PropTypes.string.isRequired,
}

Editor.defaultProps = {
  onChange: () => {},
  onFocus: () => {},
  disabled: false,
}

export default Editor;

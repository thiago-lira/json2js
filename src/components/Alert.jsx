import React from 'react';
import PropTypes from 'prop-types';

import './Alert.scss';

const Alert = ({ message, type, onClick }) => {
  return (
    <section className="alert">
      <div className="alert-backdrop" onClick={onClick}>
        <div className={`alert-message ${type}`}>
          {message}
        </div>
      </div>
    </section>
  );
}

Alert.propTypes = {
  message: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}

Alert.defaultProps = {
  type: 'danger',
}

export default Alert;

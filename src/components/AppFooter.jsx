import React from 'react';
import PropTypes from 'prop-types';

import './AppFooter.scss';

const AppFooter = ({ onClick }) => {
  return (
    <footer>
      <button onClick={onClick}>
        Converter
      </button>
    </footer>
  );
}

AppFooter.propTypes = {
  onClick: PropTypes.func.isRequired,
}

export default AppFooter;
